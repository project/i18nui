<?php
// $Id; i18nui_simple.variable.inc,v 1.1 2009/11/28 12;25;22 jareyero Exp $

/**
 * Most typical variables to be multilingual
 * 
 * They have this format so new variables are added to existing ones, but not replacing
 * $i18n_variables[] = '';
 */

$i18n_variables[] = 'site_name';
$i18n_variables[] = 'site_slogan';
$i18n_variables[] = 'site_mission';
$i18n_variables[] = 'anonymous';

// User mails
$i18n_variables[] = 'register_no_approval_required_subject';
$i18n_variables[] = 'register_no_approval_required_body';
$i18n_variables[] = 'register_admin_created_subject';
$i18n_variables[] = 'register_admin_created_body';
$i18n_variables[] = 'register_pending_approval_subject';
$i18n_variables[] = 'register_pending_approval_admin_subject';
$i18n_variables[] = 'register_pending_approval_body';
$i18n_variables[] = 'register_pending_approval_admin_body';
$i18n_variables[] = 'password_reset_subject';
$i18n_variables[] = 'password_reset_body';
$i18n_variables[] = 'status_activated_subject';
$i18n_variables[] = 'status_activated_body';
$i18n_variables[] = 'status_blocked_subject';
$i18n_variables[] = 'status_blocked_body';
$i18n_variables[] = 'status_deleted_subject';
$i18n_variables[] = 'status_deleted_body';

// Other user stuff
$i18n_variables[] = 'user_registration_help';
$i18n_variables[] = 'user_picture_guidelines';

// Date formats
$i18n_variables[] = 'date_default_timezone';
$i18n_variables[] = 'date_first_day';
$i18n_variables[] = 'date_format_long';
$i18n_variables[] = 'date_format_long_custom';
$i18n_variables[] = 'date_format_short';
$i18n_variables[] = 'date_format_short_custom';
$i18n_variables[] = 'date_format_medium';
$i18n_variables[] = 'date_format_medium_custom';
